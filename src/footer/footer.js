import React from "react";

const Footer = () => {
    return (
        <footer>
            <div className="wrapper">
                <ul className={'social'}>
                    <li className={'social-item'}>
                        <hr className={'dots dots-top'}/>
                        <a className={'social-link social-fb'} href='#facebook' target={'_blank'}> </a>
                        <hr className={'dots dots-bottom'}/>
                    </li>
                    <li className={'social-item'}>
                        <hr className={'dots dots-top'}/>
                        <a className={'social-link social-twitter'} href='#twitter' target={'_blank'}> </a>
                        <hr className={'dots dots-bottom'}/>
                    </li>
                    <li className={'social-item'}>
                        <hr className={'dots dots-top'}/>
                        <a className={'social-link social-youtube'} href='#youtube' target={'_blank'}> </a>
                        <hr className={'dots dots-bottom'}/>
                    </li>
                    <li className={'social-item'}>
                        <hr className={'dots dots-top'}/>
                        <a className={'social-link social-reddit'} href='#reddit' target={'_blank'}> </a>
                        <hr className={'dots dots-bottom'}/>
                    </li>
                </ul>
            </div>
        </footer>
    )
}

export default Footer