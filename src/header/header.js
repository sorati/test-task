import React from "react";

const Header = () => {
    return(
        <header>
            <div className="wrapper">
                <div className="header">
                    <div className="header-info">
                        <span className="logo-image"/>
                        <div className="language">
                            <span className="language-image"/>
                            <div className="language-label">ENG</div>
                        </div>
                    </div>
                    <div className="cart">
                        <a href="#cart">
                            <div className={'cart-counter'}>0</div>
                        </a>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header

